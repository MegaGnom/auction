package main

import (
	"fmt"
	"net/http"
	"runtime/debug"

	"auction/controllers"
)

func main() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("Recovered in ", string(debug.Stack()))
		}
	}()

	http.HandleFunc("/create", controllers.Create)
	http.HandleFunc("/update", controllers.Update)
	http.HandleFunc("/delete", controllers.Delete)
	http.HandleFunc("/buy", controllers.Buy)
	http.HandleFunc("/get", controllers.Get)
	http.HandleFunc("/count", controllers.Count)
	http.HandleFunc("/products", controllers.GetAllProducts)
	http.Handle("/", http.FileServer(http.Dir("static")))

	fmt.Println("Сервер запущен, перейдите по ссылке http://127.0.0.1:8181")
	http.ListenAndServe(":8181", nil)
}
