package models

import (
	"fmt"
	"os"
	"testing"
)

func TestSetStatus(t *testing.T) {
	product := Product{ID: 100, Name: "Стол", Price: 50.5, Owner: 1}
	user := User{ID: 0}
	err := product.SetStatus(Sold, user)
	if err != nil {
		t.Error(err)
	}
	if product.GetStatus() != Sold {
		t.Error("product.Status != models.Sold")
	}
}

func TestCreate(t *testing.T) {
	product := Product{ID: 100, Name: "Стол", Price: 50.5, Owner: 1}
	err := product.Create()
	if err != nil {
		t.Error(err)
	}
}

func TestGetProductsByName(t *testing.T) {
	products, err := GetProductsByName("Стол")
	if err != nil {
		t.Error(err)
	}
	if len(products) == 0 {
		t.Error("Нет найденых товаров")
	}
	fmt.Println("TestGetProductsByName ", products)

}

func TestCountProductsByOwner(t *testing.T) {
	user := User{ID: 1}
	count, err := CountProductsByOwner(user)
	if err != nil {
		t.Error(err)
	}
	if count == 0 {
		t.Error("Нет найденых товаров")
	}
	fmt.Println("TestCountProductsByOwner ", count)
}

func TestEdit(t *testing.T) {
	product := Product{ID: 100, Name: "Стол", Price: 500.5, Owner: 1}
	err := product.Update()
	if err != nil {
		t.Error(err)
	}
}

func TestBuy(t *testing.T) {
	product := Product{ID: 100, Name: "Стол", Price: 500.5, Owner: 1}
	user := User{ID: 2, Name: "Иван"}
	err := product.Buy(user)
	if err != nil {
		t.Error(err)
	}
}

func TestDelete(t *testing.T) {
	product := Product{ID: 100}
	err := product.Delete()
	if err != nil {
		t.Error(err)
	}
}

// Удаляет тестовую БД
func TestRemoveDB(t *testing.T) {
	db.Close()
	err := os.Remove("ProductDB.db")
	if err != nil {
		t.Error(err)
	}
}
