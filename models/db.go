package models

import (
	"database/sql"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

var (
	db *sql.DB = Connect()
)

func Connect() *sql.DB {
	name := "ProductDB.db"
	newDB := false
	if _, err := os.Stat(name); os.IsNotExist(err) {
		newDB = true
	}
	db, err := sql.Open("sqlite3", name)

	if err != nil {
		panic(err)
	}

	if newDB {
		_, err := db.Exec(`CREATE TABLE "Products" (
			"id"	INTEGER NOT NULL UNIQUE,
			"name"	TEXT NOT NULL,
			"price"	NUMERIC NOT NULL,
			"owner"	INTEGER NOT NULL,
			"status"	INTEGER NOT NULL,
			PRIMARY KEY("id" AUTOINCREMENT)
		);`)
		if err != nil {
			panic(err)
		}

		_, err = db.Exec(`CREATE TABLE "Users" (
			"id"	INTEGER NOT NULL UNIQUE,
			"name"	TEXT NOT NULL UNIQUE,
			PRIMARY KEY("id" AUTOINCREMENT)
		);`)
		if err != nil {
			panic(err)
		}

		_, err = db.Exec(`CREATE TABLE "Purchases" (
			"id"	INTEGER NOT NULL UNIQUE,
			"product_id"	INTEGER NOT NULL,
			"user_id"	INTEGER NOT NULL,
			PRIMARY KEY("id" AUTOINCREMENT)
		);`)
		if err != nil {
			panic(err)
		}
	}
	return db
}
