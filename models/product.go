package models

import (
	"bytes"
	"errors"
	"fmt"
	"strconv"
	"sync"
	"text/template"
)

const (
	// Активный — может купить любой пользователь, кроме владельца; владелец может сделать неактивным или удалить;
	Active = iota
	// Неактивный — нельзя купить, владелец может сделать активным или удалить;
	Inactive
	// Продан — уже купили, нельзя сделать активным, владелец может удалить;
	Sold
	// Удалён — неактивный, нельзя купить или сделать активным.
	Deleted
)

type Product struct {
	ID     int     `json:"id"`
	Name   string  `json:"nane"`
	Price  float32 `json:"price"`
	Owner  int     `json:"owner"`
	Status uint8   `json:"status"` // Для json
	status uint8   // Статус изменяется только через GetStatus, используется как основное значение
}

type Products = []Product

var (
	DBmutex sync.RWMutex
)

func SQLGenerator(text string, value interface{}) (string, error) {
	t := template.New("SQL")
	_, err := t.Parse(text)
	if err != nil {
		return "", err
	}
	buf := new(bytes.Buffer)
	err = t.Execute(buf, value)
	if err != nil {
		return "", err
	}
	sql := buf.String()

	return sql, nil
}

func NewProduct(id int, name string, price float32, owner int, status uint8) Product {
	return Product{ID: id, Name: name, Price: price, Owner: owner, Status: status, status: status}
}

func (product *Product) GetStatus() uint8 {
	product.Status = product.status
	return product.status
}

func (product *Product) SetStatus(status int, user User) error {
	switch product.status {
	case Active:
		if status == Sold && product.Owner == user.ID {
			return errors.New("может купить любой пользователь, кроме владельца")
		} else if product.Owner != user.ID && (status == Inactive || status == Deleted) {
			return errors.New("только владелец может сделать неактивным или удалить")
		}
	case Inactive:
		if product.Owner != user.ID {
			return errors.New("только владелец может сделать активным или удалить")
		}
		if status == Sold {
			return errors.New("неактивный — нельзя купить")
		}
	case Sold:
		if product.Owner != user.ID || status != Deleted {
			return errors.New("продан — уже купили, нельзя сделать активным, только владелец может удалить")
		}
	case Deleted:
		return errors.New("удалён — неактивный, нельзя купить или сделать активным")
	default:
		return errors.New("неверное значение status")
	}
	product.status = uint8(status)
	product.Status = product.status
	return nil
}

// Создание товара
func (product Product) Create() error {
	DBmutex.Lock()
	defer DBmutex.Unlock()

	text := `INSERT INTO Products ({{ if gt .ID 0 }}id, {{ end }}name, price, owner, status) 
	VALUES ({{ if gt .ID 0 }}{{.ID}},{{ end }} "{{.Name}}", {{.Price}}, {{.Owner}}, $1)`
	sql, err := SQLGenerator(text, product)
	if err != nil {
		return err
	}
	_, err = db.Exec(sql, product.status)
	if err != nil {
		return err
	}
	return nil
}

// Редактирование товара
func (product Product) Update() error {
	DBmutex.Lock()
	defer DBmutex.Unlock()
	_, err := db.Exec("UPDATE Products SET name=?, price=?, owner=?, status=? WHERE id = ?",
		product.Name,
		product.Price,
		product.Owner,
		product.status,
		product.ID)

	if err != nil {
		return err
	}
	return nil
}

// Удаление товара
func (product Product) Delete() error {
	DBmutex.Lock()
	defer DBmutex.Unlock()
	_, err := db.Exec("DELETE FROM Products WHERE id=?;",
		product.ID)
	if err != nil {
		return err
	}
	return nil
}

// Покупка товара пользователем
func (product *Product) Buy(user User) error {
	err := product.SetStatus(Sold, user)
	if err != nil {
		return err
	}
	DBmutex.Lock()
	_, err = db.Exec("INSERT INTO Purchases (product_id, user_id) VALUES ($1, $2)",
		product.ID,
		user.ID)
	DBmutex.Unlock()
	if err != nil {
		return err
	}
	err = product.Update()
	if err != nil {
		return err
	}
	return nil
}

func GetProductsByID(id int) (Product, error) {
	DBmutex.RLock()
	defer DBmutex.RUnlock()
	product := Product{}
	row := db.QueryRow("SELECT id, name, price, owner, status FROM Products WHERE id = $1", id)
	err := row.Scan(
		&product.ID,
		&product.Name,
		&product.Price,
		&product.Owner,
		&product.status,
	)
	product.GetStatus()
	if err != nil {
		return product, err
	}
	return product, nil
}

// Получить все товары
func GetAllProducts() ([]Product, error) {
	DBmutex.RLock()
	defer DBmutex.RUnlock()
	products := []Product{}
	rows, err := db.Query("SELECT id, name, price, owner, status FROM Products")
	if err != nil {
		return products, err
	}
	defer rows.Close()

	for rows.Next() {
		product := Product{}
		err := rows.Scan(&product.ID, &product.Name, &product.Price, &product.Owner, &product.status)
		if err != nil {
			fmt.Println(err)
			continue
		}
		product.GetStatus()
		products = append(products, product)
	}
	return products, nil
}

// Получение данных о товаре или наборе товаров по названию
func GetProductsByName(name string) ([]Product, error) {
	DBmutex.RLock()
	defer DBmutex.RUnlock()
	products := []Product{}
	rows, err := db.Query("SELECT id, name, price, owner, status FROM Products WHERE name = ?", name)
	if err != nil {
		return products, err
	}
	defer rows.Close()

	for rows.Next() {
		product := Product{}
		err := rows.Scan(&product.ID, &product.Name, &product.Price, &product.Owner, &product.status)
		if err != nil {
			fmt.Println(err)
			continue
		}
		product.GetStatus()
		products = append(products, product)
	}
	return products, nil
}

// Получение количества активных товаров пользователя
func CountProductsByOwner(user User) (int, error) {
	DBmutex.RLock()
	defer DBmutex.RUnlock()
	result := ""
	row := db.QueryRow("SELECT COUNT(*) FROM Products WHERE owner = ? AND status = ?", user.ID, Active)
	err := row.Scan(&result)
	if err != nil {
		return 0, err
	}
	count, err := strconv.Atoi(result)
	if err != nil {
		return 0, err
	}
	return count, nil
}
