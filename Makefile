help:
	@echo "Команды: build - собрать проект, run - запустить проект"

build:
	go build -o .

run:
	go run .