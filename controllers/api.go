package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"auction/models"
)

func Create(w http.ResponseWriter, r *http.Request) {
	// if r.Method != "POST" {
	// 	w.WriteHeader(http.StatusBadRequest)
	// 	return
	// }
	id, err := strconv.Atoi(r.URL.Query().Get("id"))
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusBadRequest)
		return
	}
	name := r.URL.Query().Get("name")
	price, err := strconv.ParseFloat(r.URL.Query().Get("price"), 32)
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusBadRequest)
		return
	}
	owner, err := strconv.Atoi(r.URL.Query().Get("owner"))
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusBadRequest)
		return
	}
	status, err := strconv.Atoi(r.URL.Query().Get("status"))
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusBadRequest)
		return
	}

	product := models.NewProduct(id, name, float32(price), owner, uint8(status))

	err = product.Create()
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, `{"status":"OK"}`)
}

func Update(w http.ResponseWriter, r *http.Request) {
	// if r.Method != "POST" {
	// 	w.WriteHeader(http.StatusBadRequest)
	// 	return
	// }
	id, err := strconv.Atoi(r.URL.Query().Get("id"))
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusBadRequest)
		return
	}
	name := r.URL.Query().Get("name")
	price, err := strconv.ParseFloat(r.URL.Query().Get("price"), 32)
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusBadRequest)
		return
	}
	owner, err := strconv.Atoi(r.URL.Query().Get("owner"))
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusBadRequest)
		return
	}
	status, err := strconv.Atoi(r.URL.Query().Get("status"))
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusBadRequest)
		return
	}
	// Проверяем, что товар с id есть в БД
	_, err = models.GetProductsByID(id)
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusBadRequest)
		return
	}
	product := models.NewProduct(id, name, float32(price), owner, uint8(status))
	err = product.Update()
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, `{"status":"OK"}`)
}

func Delete(w http.ResponseWriter, r *http.Request) {
	// if r.Method != "DELETE" {
	// 	w.WriteHeader(http.StatusBadRequest)
	// 	return
	// }
	id, err := strconv.Atoi(r.URL.Query().Get("id"))
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusBadRequest)
		return
	}

	product := models.Product{ID: id}
	err = product.Delete()
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, `{"status":"OK"}`)
}

func Buy(w http.ResponseWriter, r *http.Request) {
	// if r.Method != "POST" {
	// 	w.WriteHeader(http.StatusBadRequest)
	// 	return
	// }
	productID, err := strconv.Atoi(r.URL.Query().Get("productid"))
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusBadRequest)
		return
	}
	userID, err := strconv.Atoi(r.URL.Query().Get("userid"))
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusBadRequest)
		return
	}

	name := r.URL.Query().Get("name")

	product, err := models.GetProductsByID(productID)
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusBadRequest)
		return
	}

	user := models.User{ID: userID, Name: name}
	err = product.Buy(user)
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, `{"status":"OK"}`)
}

func Get(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	name := r.URL.Query().Get("name")

	products, err := models.GetProductsByName(name)
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusInternalServerError)
		return
	}

	js, err := json.Marshal(products)
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func Count(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	id, err := strconv.Atoi(r.URL.Query().Get("id"))
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusBadRequest)
		return
	}
	user := models.User{ID: id}

	count, err := models.CountProductsByOwner(user)
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusInternalServerError)
		return
	}

	response := fmt.Sprintf(`{"status":"OK", "count":%d}`, count)
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprint(w, response)
}

func GetAllProducts(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	products, err := models.GetAllProducts()
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusInternalServerError)
		return
	}

	js, err := json.Marshal(products)
	if err != nil {
		http.Error(w, fmt.Sprintf(`{"status":"error", "error":"%s"}`, err.Error()), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
